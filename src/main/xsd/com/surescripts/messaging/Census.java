//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2017.06.14 at 06:59:27 PM IST 
//


package com.surescripts.messaging;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Request" type="{http://www.surescripts.com/messaging}CensusRequestType"/>
 *         &lt;element name="Prescriber" type="{http://www.surescripts.com/messaging}PrescriberRxFillType" minOccurs="0"/>
 *         &lt;element name="Facility" type="{http://www.surescripts.com/messaging}MandatoryFaciltyType"/>
 *         &lt;element name="Patient" type="{http://www.surescripts.com/messaging}PatientType"/>
 *         &lt;element name="BenefitsCoordination" type="{http://www.surescripts.com/messaging}CensusBenefitsCoordinationType" maxOccurs="3" minOccurs="0"/>
 *         &lt;element ref="{http://www.surescripts.com/messaging}Allergy" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://www.surescripts.com/messaging}DiagnosisGeneral" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "request",
    "prescriber",
    "facility",
    "patient",
    "benefitsCoordinations",
    "allergies",
    "diagnosisGenerals"
})
@XmlRootElement(name = "Census")
public class Census
    implements Serializable
{

    private final static long serialVersionUID = -1L;
    @XmlElement(name = "Request", required = true)
    protected CensusRequestType request;
    @XmlElement(name = "Prescriber")
    protected PrescriberRxFillType prescriber;
    @XmlElement(name = "Facility", required = true)
    protected MandatoryFaciltyType facility;
    @XmlElement(name = "Patient", required = true)
    protected PatientType patient;
    @XmlElement(name = "BenefitsCoordination")
    protected List<CensusBenefitsCoordinationType> benefitsCoordinations;
    @XmlElement(name = "Allergy")
    protected List<Allergy> allergies;
    @XmlElement(name = "DiagnosisGeneral")
    protected List<DiagnosisGeneral> diagnosisGenerals;

    /**
     * Gets the value of the request property.
     * 
     * @return
     *     possible object is
     *     {@link CensusRequestType }
     *     
     */
    public CensusRequestType getRequest() {
        return request;
    }

    /**
     * Sets the value of the request property.
     * 
     * @param value
     *     allowed object is
     *     {@link CensusRequestType }
     *     
     */
    public void setRequest(CensusRequestType value) {
        this.request = value;
    }

    /**
     * Gets the value of the prescriber property.
     * 
     * @return
     *     possible object is
     *     {@link PrescriberRxFillType }
     *     
     */
    public PrescriberRxFillType getPrescriber() {
        return prescriber;
    }

    /**
     * Sets the value of the prescriber property.
     * 
     * @param value
     *     allowed object is
     *     {@link PrescriberRxFillType }
     *     
     */
    public void setPrescriber(PrescriberRxFillType value) {
        this.prescriber = value;
    }

    /**
     * Gets the value of the facility property.
     * 
     * @return
     *     possible object is
     *     {@link MandatoryFaciltyType }
     *     
     */
    public MandatoryFaciltyType getFacility() {
        return facility;
    }

    /**
     * Sets the value of the facility property.
     * 
     * @param value
     *     allowed object is
     *     {@link MandatoryFaciltyType }
     *     
     */
    public void setFacility(MandatoryFaciltyType value) {
        this.facility = value;
    }

    /**
     * Gets the value of the patient property.
     * 
     * @return
     *     possible object is
     *     {@link PatientType }
     *     
     */
    public PatientType getPatient() {
        return patient;
    }

    /**
     * Sets the value of the patient property.
     * 
     * @param value
     *     allowed object is
     *     {@link PatientType }
     *     
     */
    public void setPatient(PatientType value) {
        this.patient = value;
    }

    /**
     * Gets the value of the benefitsCoordinations property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the benefitsCoordinations property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBenefitsCoordinations().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CensusBenefitsCoordinationType }
     * 
     * 
     */
    public List<CensusBenefitsCoordinationType> getBenefitsCoordinations() {
        if (benefitsCoordinations == null) {
            benefitsCoordinations = new ArrayList<CensusBenefitsCoordinationType>();
        }
        return this.benefitsCoordinations;
    }

    /**
     * Gets the value of the allergies property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the allergies property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAllergies().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Allergy }
     * 
     * 
     */
    public List<Allergy> getAllergies() {
        if (allergies == null) {
            allergies = new ArrayList<Allergy>();
        }
        return this.allergies;
    }

    /**
     * Gets the value of the diagnosisGenerals property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the diagnosisGenerals property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDiagnosisGenerals().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DiagnosisGeneral }
     * 
     * 
     */
    public List<DiagnosisGeneral> getDiagnosisGenerals() {
        if (diagnosisGenerals == null) {
            diagnosisGenerals = new ArrayList<DiagnosisGeneral>();
        }
        return this.diagnosisGenerals;
    }

}
