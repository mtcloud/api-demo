package com.ims;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LoadtestApplication {

    public static void main(String[] args) {

        SpringApplication.run(LoadtestApplication.class, args);
        // final int IsSingle = 1 << 0;
        // final int IsGraduate = 1 << 1;
        // final int IsMale = 1 << 2;
        // final int IsLookingForPartner = 1 << 3;
        // System.out.println(IsSingle);
        // System.out.println(IsGraduate);
        // System.out.println(IsMale);
        // System.out.println(IsLookingForPartner);
        // int Value = 0;
        // if ((Value & IsSingle) != 0) {
        // System.out.println(Value);
        // }
        // if ((Value & IsGraduate) != 0) {
        // System.out.println(Value);
        // }
        // // Set "Single"
        // Value |= IsSingle;
        // Value |= IsGraduate;
        // System.out.println(Value);
        // // Remove "Graduate"
        // Value &= ~IsGraduate;
        // System.out.println(Value);
        // Value &= ~IsSingle;
        // System.out.println(Value);
    }
    // @Bean // this could be provided via auto-configuration
    // MethodValidationPostProcessor methodValidationPostProcessor() {
    //
    // return new MethodValidationPostProcessor();
    // }
}
