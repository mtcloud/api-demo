package com.ims;

import java.util.HashMap;
import java.util.Map;

import org.fluentd.logger.FluentLogger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * Project loadtest
 *
 * @author ankura
 * @version 1.0
 * @date Mar 29, 2017
 */
@RestController
@RequestMapping(value = "/")
public class TestController {

    public static final Logger logger = LoggerFactory.getLogger(TestController.class);

    private static FluentLogger LOG = FluentLogger.getLogger("mps", "104.154.227.100", 24224);

    @RequestMapping(value = "test", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE,
                    produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ResponseStatus(value = HttpStatus.OK)
    public Map get() {

        logger.info("Info from laodtest app");
        logger.error("Error from loadtest app");
        logger.trace("Trace from loadtest app");
        logger.debug("Debug from loadtest app");
        // LOG.log("testController", "INFO", "Log from LoadTestApp");
        Map map = new HashMap<>();
        map.put("Key", "Loaded");
        return map;
    }
}
